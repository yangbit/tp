# _*_ coding:utf-8 _*_

import getWinningNum
import getKillTrend as gkt
import itchat
import random
import getOmit
import re


def main(num, weixin, killreds=',', killblues=','):



    red = [i for i in xrange(34)]
    blue = [n for n in xrange(17)]
    # 获取并更新中奖数据
    # 读取本地文件,并融合新中奖号码
    wins = getWinningNum.GetDoubleColorBallNumber().get()
    # 获取最新一期杀号数据
    kills = gkt.GetKillTrend().get(num)
    # 获取最新网易红球冷号
    coldr = getOmit.getCold()
    # 杀号
    red1 = []
    blue1 = []

    # 手动杀红球
    if (len(killreds) > 2):
        red1 = killreds.split(',')
        for item in red1[:]:
            if int(item) in red:
                red.remove(int(item))
    # 自动杀红球
    if (len(killreds) < 2):
        for i in (1, 3, 5):
            red1 = kills[i].split(',')[3:]
            for item in red1[:]:
                if int(item) in red:
                    red.remove(int(item))
    # 自动杀蓝球
    for i in (0, 2, 4):
        blue1 = kills[i].split(',')[3:]
        for item in blue1[:]:
            if int(item) in blue:
                blue.remove(int(item))
    # 手工杀蓝球
    if (len(killblues) > 1):
        blue2 = killblues.split(',')
        for item in blue2[:]:
            if int(item) in blue:
                blue.remove(int(item))

    redkilled = [i for i in xrange(34)]
    for item in red[1:]:
        if item in redkilled[1:]:
            redkilled.remove(int(item))
    bluekilled = [n for n in xrange(17)]
    for item in blue[1:]:
        if item in bluekilled[1:]:
            bluekilled.remove(int(item))

    sends = '保留红球' + str(len(red) - 1) + '个: ' + ', '.join(map(str, red[1:]))+ ' \n'
    sends = sends + '保留蓝球' + str(len(blue) - 1) + '个: ' + ', '.join(map(str, blue[1:])) + ' \n'
    sends = sends + '红球杀号' + str(len(redkilled) - 1) + '个: ' + ', '.join(map(str, redkilled[1:]))+ ' \n'
    sends = sends + '蓝球杀号' + str(len(bluekilled) - 1) + '个: ' + ', '.join(map(str, bluekilled[1:]))+ ' \n'
    sends = '双色球第' + str(num) + '期:全杀号方案\n' + sends
    print sends


    # 读取文件
    f = open('ssq.txt')
    fp = f.read()
    f.close()

    # 全杀号选注
    se1 =''
    for i in xrange(3):
        r1 = random.sample(red[1:], 6)
        b1 = random.sample(blue[1:], 1)
        r1.sort()
        r = []
        b = []
        for ritem in r1:
            r.append('{0:02}'.format(ritem))
        for bitem in b1:
            b.append('{0:02}'.format(bitem))
        rr = ', '.join(map(str, r))
        bb = ', '.join(map(str, b))
        if len(re.findall((rr+' '+bb).replace(', ',' '), fp)) == 0:
            se1 = se1 + rr + ' : ' + bb + '\n'
    print '杀号选号3：\n' + se1

    # 全杀号+杀红球冷号
    for item in coldr:
        if item in red:
            red.remove(item)
    se1c =''
    for i in xrange(2):
        r1 = random.sample(red[1:], 6)
        b1 = random.sample(blue[1:], 1)
        r1.sort()
        r = []
        b = []
        for ritem in r1:
            r.append('{0:02}'.format(ritem))
        for bitem in b1:
            b.append('{0:02}'.format(bitem))
        rr = ', '.join(map(str, r))
        bb = ', '.join(map(str, b))
        if len(re.findall((rr+' '+bb).replace(', ',' '), fp)) == 0:
            se1c = se1c + rr + ' : ' + bb + '\n'
    print '杀号（含冷号）选号2：\n' + se1c

    if weixin:
        # 通过如下命令登陆，即使程序关闭，一定时间内重新开启也可以不用重新扫码。
        itchat.auto_login(hotReload=True)
        itchat.send(sends, toUserName='yxl18673100506')
        itchat.send(se1 + se1c, toUserName='yxl18673100506')

if __name__ == '__main__':
    Get = main('2017039', True, '03,04,06,08,02,26,13,18,21', '04')
    # 下一步,逻辑:选三家红球杀号方案中的一项和手工杀红球组合.
    # Get = main('2017039',False,)
